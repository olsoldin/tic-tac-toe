package game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TTTFrame extends JFrame{

    private TicTacToePanel tictactoe;

    public TTTFrame(){
        super("Tic Tac Toe");
        setMinimumSize(new Dimension(610, 670));
        setSize(600, 670);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        tictactoe = new TicTacToePanel();
        add(tictactoe, BorderLayout.CENTER);

        JPanel bottom = new JPanel();
        bottom.setBackground(Color.darkGray);
        JButton resetButton = new JButton("Reset");
        add(bottom, BorderLayout.SOUTH);
        add(resetButton, BorderLayout.SOUTH);

        resetButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                tictactoe.reset();
                tictactoe.requestFocus();
            }
        });

        tictactoe.requestFocus();
        setVisible(true);
    }
}