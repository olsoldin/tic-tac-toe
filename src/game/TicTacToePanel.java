package game;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class TicTacToePanel extends JPanel{

    public static final int PANEL_WIDTH = 600;
    public static final int PANEL_HEIGHT = 600;
    public static final int GRID_WIDTH = PANEL_WIDTH / 3;
    public static final int GRID_HEIGHT = PANEL_HEIGHT / 3;
    public int[][] board = new int[3][3]; //0 = blank; 1 = X; 2 = Y
    public int player = 1;
    public boolean won = false;
    private Image xImage;
    private Image oImage;
    private TTTAi AIplayer = new TTTAi();
    private JPanel ox;


    public static void main(String[] args){
        new TTTFrame();
    }


    public TicTacToePanel(){
        setLayout(new BorderLayout());
        setSize(PANEL_WIDTH, PANEL_HEIGHT);
        ox = new JPanel();

        for(int x = 0; x < 3; x++){
            for(int y = 0; y < 3; y++){
                board[x][y] = 0;
            }
        }

        xImage = Toolkit.getDefaultToolkit().getImage("Assets/x.png");
        oImage = Toolkit.getDefaultToolkit().getImage("Assets/o.png");

        addMouseListener(new MouseHandler(this));
    }


    public void reset(){
        won = false;
        player = 1;
        for(int x = 0; x < board.length; x++){
            for(int y = 0; y < board.length; y++){
                board[x][y] = 0;
            }
        }
        repaint();
    }


    @Override
    public void paintComponent(Graphics g){
        if(xImage == null || oImage == null){
            System.exit(1);
        }
        paintGrid(g);
        paintGame(g);
    }


    private void paintGrid(Graphics g){
        g.drawLine(5, GRID_HEIGHT, PANEL_WIDTH - 5, GRID_HEIGHT);
        g.drawLine(5, 2 * GRID_HEIGHT, PANEL_WIDTH - 5, 2 * GRID_HEIGHT);

        g.drawLine(GRID_WIDTH, 5, GRID_WIDTH, PANEL_HEIGHT - 5);
        g.drawLine(2 * GRID_WIDTH, 5, 2 * GRID_WIDTH, PANEL_HEIGHT - 5);
    }


    private void paintGame(Graphics g){
        g.setColor(Color.CYAN);

        for(int x = 0; x < 3; x++){
            for(int y = 0; y < 3; y++){
                if(board[x][y] == 1){
                    g.drawImage(xImage, x * GRID_WIDTH, y * GRID_HEIGHT, null);
                }else if(board[x][y] == 2){
                    g.drawImage(oImage, x * GRID_WIDTH, y * GRID_HEIGHT, null);
                }else{
                    g.fillRect(x * GRID_WIDTH + 1, y * GRID_HEIGHT + 1,
                            GRID_WIDTH - 1, GRID_HEIGHT - 1);
                }
            }
        }
    }

    public class MouseHandler implements MouseListener{

        private TicTacToePanel pane;


        public MouseHandler(TicTacToePanel panel){
            pane = panel;
        }


        @Override
        public void mouseClicked(MouseEvent e){
            if(!won){
                singlePlayer(e);
                aiPlayer();
                checkWin();
            }
        }


        public void takeGo(int column, int row){
            if(board[column][row] == 0){
                board[column][row] = player;
                checkWin();
                if(player == 1){
                    player = 2;
                }else{
                    player = 1;
                }
            }
        }


        private void aiPlayer(){
            pane.AIplayer.calculatePosition(board);
            takeGo(pane.AIplayer.getColumn(), pane.AIplayer.getRow());
        }


        private void singlePlayer(MouseEvent e){
            int column = e.getX() / GRID_WIDTH;
            int row = e.getY() / GRID_HEIGHT;
            takeGo(column, row);
            repaint();
        }


        @Override
        public void mousePressed(MouseEvent e){
        }


        @Override
        public void mouseReleased(MouseEvent e){
        }


        @Override
        public void mouseEntered(MouseEvent e){
        }


        @Override
        public void mouseExited(MouseEvent e){
        }


        private void checkWin(){
            for(int y = 0; y < board.length; y++){
                boolean rowWin = true;
                for(int x = 0; x < board[y].length; x++){
                    if(board[x][y] != player){
                        rowWin = false;
                    }
                }
                if(rowWin){
                    displayWin();
                    return;
                }
            }

            for(int y = 0; y < board.length; y++){
                boolean rowWin = true;
                for(int x = 0; x < board[y].length; x++){
                    if(board[y][x] != player){
                        rowWin = false;
                    }
                }
                if(rowWin){
                    displayWin();
                    return;
                }
            }

            boolean topWin = true;
            boolean bottomWin = true;
            for(int y = 0; y < board.length; y++){
                if(board[y][y] != player){
                    topWin = false;
                }
                if(board[(board.length - (y + 1))][y] != player){
                    bottomWin = false;
                }
            }
            if(topWin || bottomWin){
                displayWin();
                return;
            }

            boolean isFull = true;
            for(int y = 0; y < board.length; y++){
                for(int x = 0; x < board[y].length; x++){
                    if(board[x][y] == 0){
                        isFull = false;
                    }
                }
            }
            if(isFull){
                repaint();
                JOptionPane.showMessageDialog(pane, "Tie!", "", JOptionPane.INFORMATION_MESSAGE);
            }
        }


        private void displayWin(){
            won = true;
            repaint();
            JOptionPane.showMessageDialog(pane, "Player " + player + " wins!", "Winner", JOptionPane.INFORMATION_MESSAGE);
            repaint();
        }

    }
}
