/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game2;

/**
 *
 * @author Oliver
 */
public class TicTacToe {

    private static final int EMPTY = -1;
    private static final int PLAYER_X = 1, PLAYER_Y = 0;
    private int[] board = new int[9];

    public TicTacToe() {
        reset();
        System.out.println(this);
    }

    public void reset() {
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                board[x * 3 + y] = EMPTY;
            }
        }
    }

    public boolean choose(int x, int y, boolean isPlayerX) {
        return choose(x * 3 + y, isPlayerX);
    }

    public boolean choose(int xy, boolean isPlayerX) {
        if (isFreeSpace(xy)) {
            board[xy] = isPlayerX ? PLAYER_X : PLAYER_Y;
            System.out.println(toString());
            return true;
        }
        return false;
    }

    public boolean isFreeSpace(int xy) {
        return board[xy] == -1;
    }

    /**
     *
     * @return -1 if nothing, 1 if X win, 0 if Y win, 2 if tie
     */
    public int checkWin() {

        //Check columns
        for (int y = 0; y < 3; y++) {
            if (board[y] != -1 && board[y] == board[3 + y] && board[3 + y] == board[6 + y]) {
                return board[y];
            }
        }
        //Check rows
        for (int x = 0; x < 3; x++) {
            if (board[x * 3] != -1 && board[x * 3] == board[x * 3 + 1] && board[x * 3 + 1] == board[x * 3 + 2]) {
                return board[x * 3];
            }
        }

        //Check right diagonal
        if (board[0] != -1 && board[0] == board[4] && board[4] == board[8]) {
            return board[0];
        }
        //Check left diagonal
        if (board[2] != -1 && board[2] == board[4] && board[4] == board[6]) {
            return board[2];
        }
        
        //Check if the board is full
        for(int i = 0; i < 9; i++){
            if(board[i] == -1){
                return -1;
            }
        }

        return 2;
    }

    @Override
    public String toString() {
        String result = "";
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                result += min2chars(board[x * 3 + y]) + " ";
            }
            result += "\n";
        }
        return result;
    }

    public String get(int i) {
        switch (board[i]) {
            case 0:
                return "Y";
            case 1:
                return "X";
            default:
                return "";
        }
    }

    /**
     * Returns the object as a string with the minimum of 2 characters
     *
     * @param obj the object to return as a string with more than 2 chars
     *
     * @return String with at least 2 characters
     */
    private static String min2chars(Object obj) {
        String str = obj.toString();
        return str.length() <= 1 ? (str.length() == 0 ? "  " : " " + str) : str;
    }
}
