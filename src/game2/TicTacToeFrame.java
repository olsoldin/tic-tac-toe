/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game2;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Oliver
 */
public class TicTacToeFrame extends JFrame {

    private boolean isPlayerX = true;
    private TicTacToe game = new TicTacToe();
    private JButton[] buttons = new JButton[9];

    public static void main(String[] args) {
        new TicTacToeFrame();
    }

    public TicTacToeFrame() {
        this.setTitle("Tic Tac Toe");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GridLayout layout = new GridLayout(3, 3);
        this.setLayout(layout);
        this.setPreferredSize(new Dimension(300, 300));
        initButtons();
        this.pack();
        this.setVisible(true);
    }

    private void initButtons() {
        for (int i = 0; i < 9; i++) {
            buttons[i] = new JButton();
            buttons[i].setName(i + "");
            buttons[i].setFont(new Font(buttons[i].getFont().getName(), Font.PLAIN, 20));
            buttons[i].addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    JButton button = (JButton) e.getSource();
                    if (game.choose(Integer.parseInt(button.getName()), isPlayerX)) {
                        isPlayerX = !isPlayerX;
                        updateButtons();
                        button.setEnabled(false);
                        int win = game.checkWin();
                        if (win != -1) {
                            System.out.println(game.checkWin());
                            String message = "";
                            switch (win) {
                                case 0:
                                    message = "Player y wins!";
                                    break;
                                case 1:
                                    message = "Player x wins!";
                                    break;
                                case 2:
                                    message = "Game is a tie";
                                    break;
                            }
                            int option = JOptionPane.showConfirmDialog(null, message + "\nPlay again?");
                            if(option == JOptionPane.OK_OPTION){
                                game.reset();
                                resetButtons();
                                updateButtons();
                            }
                        }
                    }
                }
            });
            add(buttons[i]);
        }
    }
    
    private void resetButtons(){
        for(int i = 0; i < 9; i++){
            buttons[i].setEnabled(true);
        }
    }

    private void updateButtons() {
        for (int i = 0; i < 9; i++) {
            buttons[i].setText(game.get(i));
        }
    }
}
